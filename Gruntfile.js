/**
 * Created by Lindon Camaj on 6/5/17.
 */

module.exports = function (grunt) {

    // node.js path component
    var path = require('path');

    require('time-grunt')(grunt);

    // load grunt tasks
    require('load-grunt-config')(grunt, {

        // path to task.js files, defaults to grunt dir
        configPath: path.join(process.cwd(), 'config'),

        // auto grunt.initConfig
        init: true,

        // data passed into config.  Can use with <%= test %>
        data: {
            test: false
        },

        // use different function to merge config files
        mergeFunction: require('recursive-merge'),

        jitGrunt: {
            // here you can pass options to jit-grunt (or just jitGrunt: true)
            staticMappings: {
                // here you can specify static mappings, for example:
                ngtemplates: 'grunt-angular-templates'
            }
        },

        //can post process config object before it gets passed to grunt
        postProcess: function(config) {},

        //allows to manipulate the config object before it gets merged with the data object
        preMerge: function(config, data) {}

    });

    grunt.registerTask('serve', 'Application build and start server', function(target){

        grunt.task.run([
            'clean',
            'sass',
            'includeSource:dev',
            'wiredep:dev',
            'express',
            'watch'
        ]);

    });

    grunt.registerTask('buildDev', ['clean', 'sass', 'ngtemplates', 'includeSource:dev', 'wiredep:dev']);

    grunt.registerTask('build', ['clean', 'sass', 'jshint', 'concat', 'uglify', 'imagemin', 'ngtemplates', 'includeSource:dev', 'wiredep:dev'])

    grunt.registerTask('dev', ['buildDev', 'express', 'watch']);
};
