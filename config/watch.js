/**
 * Created by Lindon Camaj on 6/6/17.
 */
module.exports = {

    options: {
        livereload: {
            host: 'localhost',
            port: 35729
        }
    },

    // watch angular js files
    angular: {
        files: ['src/js/app/**/*.{js,html}'],
        tasks: ['jshint', 'ngtemplates', 'includeSource:dev', 'wiredep:dev']
    },

    // watch javascript files
    js: {
        files: ['src/js/api/**/*.js'],
        tasks: ['jshint', 'includeSource:dev', 'wiredep:dev']
    },

    // watch sass files and generate css files
    sass: {
        files: ['src/assets/sass/**/*.scss'],
        tasks: ['sass']
    }
};