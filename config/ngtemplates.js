/**
 * Created by Lindon Camaj on 6/6/17.
 */
module.exports = {
    synergySuite:{
        cwd: "src/js/app/",
        src: "**/*.html",
        dest: "src/js/app/app.templates.js",
        options:{
            htmlmin: { collapseWhitespace: true, collapseBooleanAttributes: true, removeComments: true }
        }
    }
};
