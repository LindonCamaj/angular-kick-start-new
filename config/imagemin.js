/**
 * Created by Lindon Camaj on 6/6/17.
 */
module.exports = {
    dev: {
        options: {
            optimizationLevel: 5,
        },
        files: [{
            expand: true,
            cwd: 'src/assets/images/',
            src: ['**/*.{png,jpg,gif}'],
            dest: 'dist/assets/images/'
        }]
    }
};