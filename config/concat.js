/**
 * Created by Lindon Camaj on 6/6/17.
 */
module.exports = {
    angular: {
        src: ['src/js/app/app.js', 'src/js/app/app.templates.js', 'src/js/app/**/*.module.js', 'src/js/app/**/*.controller.js', 'src/js/app/**/*.service.js', 'src/js/app/**/*.directive.js'],
        dest: 'dist/js/app.js'
    },
    api: {
        src: ['src/js/api/**/*.js'],
        dest: 'dist/js/api.js'
    }
};