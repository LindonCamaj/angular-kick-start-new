/**
 * Created by Lindon Camaj on 6/5/17.
 */
module.exports = {
    options: {
        basePath: '',
        typeMappings: {
            'html': 'html'
        }
    },

    dev: {
        files: {
            'index.html': 'src/index/index_dev.html'
        }
    }
};