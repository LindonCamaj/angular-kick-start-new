/**
 * Created by Lindon Camaj on 6/6/17.
 */
module.exports = {
    options: {
        beautify : false,
        mangle: false,
        compress: true,
        sourceMap: false
    },

    angular:{
        files: {
            'dist/js/app.min.js': ['<%= concat.angular.dest %>']
        }
    },

    api:{
        files: {
            'dist/js/api.min.js': ['<%= concat.api.dest %>']
        }
    }
};