/**
 * Created by Lindon Camaj on 6/5/17.
 */
module.exports = {
    dist: {
        options: {
            noCache: true,
            style: 'compressed'
        },
        files: {
            'dist/assets/css/main.css': 'src/assets/sass/main.scss'
        }
    }
};