/**
 * Created by Lindon Camaj on 6/6/17.
 */
module.exports = {
    livereloadServer: {
        options: {
            port: '9000',
            bases: './',
            livereload: true, // if you just specify `true`, default port `35729` will be used
        }
    }
};