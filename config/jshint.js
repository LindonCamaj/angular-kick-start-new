/**
 * Created by Lindon Camaj on 6/6/17.
 */

module.exports = {
    app: ['src/js/**/*.js'],
    options: {
        notypeof: true,
        debug: true,
        eqnull: true,
        eqeqeq: false,
        globals: {
            jQuery: true,
            console: true,
            module: true,
            document: true
        }
    }
};